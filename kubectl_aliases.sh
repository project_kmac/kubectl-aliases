#!/bin/bash
# install kubectl aliases
# wget https://gitlab.com/project_kmac/kubectl-aliases/-/raw/master/.kubectl_aliases

mv .kubectl_aliases ../
echo " [ -f ~/.kubectl_aliases ] && source ~/.kubectl_aliases " >> ~/.bashrc

alias ls="ls -la"

printf "set tabstop=2 \n set expandtab \n set shiftwidth=2" >> ~/.vimrc

# echo " [ -f ~/.kubectl_aliases ] && source ~/.kubectl_aliases " >> ~/.zshrc
# source ~/.bashrc
# source ~/.zshrc
